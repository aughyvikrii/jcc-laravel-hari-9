<?php

require_once "./animal.php";

class Frog extends Animal {

    public $legs = 4;
    public $cold_blooded = "no";
    public $jump = "Hop Hop";

    function __construct($name = null) {
        $this->name = $name;
    }

    public function jump() {
        return $this->jump;
    }
}