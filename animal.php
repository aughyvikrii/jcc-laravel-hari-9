<?php

class Animal {

    public $name;
    public $legs;
    public $cold_blooded;
    public $jump;
    public $yell;

    function __construct($name = null) {
        if(!empty($name)) {
            $this->initAnimal($name);
        }
    }

    public function initAnimal($name) {
        $this->name = $name;
        $this->legs = 4;
        $this->cold_blooded = "no";
    }

    public function name() {
        return $this->name;
    }

    public function legs() {
        return $this->legs;
    }

    public function cold_blooded() {
        return $this->cold_blooded;
    }

    public function __get($name) {
        return isset($this->$name) ? $this->$name : null;
    }
}